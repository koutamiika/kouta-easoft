<?php
/**
 * Plugin Name: WooCommerce Easoft
 * Description: Easoft -integraatio
 * Version: 1.0
 * Author: Kouta Media
 *
 * @package woocommerce-easoft
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'EASOFT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once EASOFT_PLUGIN_DIR . '/admin/class-woocommerce-easoft-admin.php';
require_once EASOFT_PLUGIN_DIR . '/inc/class-woocommerce-easoft.php';

/**
 * Initialize the plugin.
 */
function woocommerce_easoft_init() {
	if ( class_exists( 'WooCommerce' ) ) {
		$easoft = new WooCommerce_Easoft();
	}
}
add_action( 'init', 'woocommerce_easoft_init' );

// function easoft_api_call( $order_id ) {

// 	// Bail if no api credentials.
// 	// TODO: Add warning to the backend.
// 	if ( ! $api_user || ! $api_pw || ! $api_url) {
// 		return;
// 	}

// 	// Order Setup Via WooCommerce.
// 	$order = wc_get_order( $order_id );

// 	// Iterate Through Items.
// 	// TODO: Get variable items also.
// 	$items = $order->get_items();

// 	$order_items = array();
// 	foreach ( $items as $item ) {

// 		$product = $item->get_product();

// 		$order_items[] = array(
// 			'item'        => $product->get_sku(),
// 			'description' => $product->get_name(),
// 			'freetext'    => '',
// 			'qty'         => $item->get_quantity(),
// 			'price'       => number_format( $item->get_total(), 2, '.', '' ),
// 			'discount'    => $product->get_sale_price(),
// 			'vat_rate'    => 24,
// 			'unit'        => 'kpl',
// 			'type'        => 0,
// 		);

// 	}

// 	$body = array(
// 		'Order'       => array(
// 			'seller_username'       => get_option( 'woocommerce-easoft-seller-username' ),                         // REQUIRED. Myyjän yksilöivä käyttäjätunnus, joka tulee löytyä Easoftin järjestelmästä.
// 			'date'                  => $order->get_date_created(),                                                 // REQUIRED.Tarjouspäivä, joka tulee olla ISO 8601-muodossa (YYYY-MM-YY).
// 			'approval_date'         => $order->get_date_completed(),                                               // REQUIRED IF STATUS = 2. Tilauspäivä, joka tulee olla ISO 8601-muodossa (YYYY-MM-YY).
// 			'status'                => 2,                                                                          // REQUIRED. Tilauksen tila. 2 = tilaus, 1 = tarjous.
// 			'your_ref'              => '',                                                                         // Asiakkaan viite -tieto. max 255 merkkiä.
// 			'our_ref'               => '',                                                                         // Myyjän viite -tieto. max 255 merkkiä.
// 			'payment_term_code'     => '',                                                                         // Maksuehtokoodi. Tällä koodilla tulee löytyä Easoftista maksuehto.
// 			'customer_order_number' => $order->get_order_number(),
// 			'customer_job_number'   => '',
// 			'notes'                 => $order->get_customer_note(),
// 			'internal_notes'        => '',
// 			'recording_type'        => 2,                                                                          // 1 = Brutto, 2 = Netto
// 			'customer_id'           => ( $order->get_customer_id() ? $order->get_customer_id() : null ),
// 			'customer_name'         => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
// 			'customer_name2'        => '',
// 			'customer_address'      => $order->get_billing_address_1(),
// 			'customer_postcode'     => $order->get_billing_postcode(),
// 			'customer_locality'     => $order->get_billing_city(),
// 			'customer_phone'        => $order->get_billing_phone(),
// 			'customer_phone2'       => '',
// 			'customer_einvoiceid'   => '',
// 			'customer_evoiceint'    => '',
// 			'customer_label_code'   => '',
// 			'customer_group_code'   => '',
// 			'customer_type'         => 2,
// 			'invoicing_customer'    => true,
// 			'invoicing_name'        => '',
// 			'invoicing_address'     => '',
// 			'invoicing_postcode'    => '',
// 			'invoicing_locality'    => '',
// 			'invoicing_phone'       => '',
// 			'invoicing_email'       => $order->get_billing_email(),
// 			'target_price'          => '',
// 			'target_type'           => '',
// 			'installed'             => 2,
// 			'fixed_price_deal'      => 2,
// 			'desired_delivery_date' => 2,
// 			'hdc_print'             => 0,
// 			'customer_label_id'     => 'Customer Label ID',
// 			'sale_region_code'      => '',
// 		),
// 		'OrderRow'    => $order_items,
// 		'Attachments' => array(
// 			'name'      => '',
// 			'own_ref'   => '',
// 			'mime_type' => '',
// 			'data'      => null,
// 		),
// 	);

// 	// TODO: API call requires Basic auth.
// 	$response = wp_remote_post( 
// 		$api_url,
// 		array(
// 			'headers' => array(
// 				'Content-Type' => 'application/json; charset=utf-8',
// 			),
// 			'method'  => 'POST',
// 			'timeout' => 75,
// 			'body'    => wp_json_encode( $body ),
// 		)
// 	);

// 	if ( is_wp_error( $response ) ) {
// 		$error = $response->get_error_message();
// 	} else {
// 		$order->add_order_note( 'Tilaus luotu onnistuneesti Easoftiin' );
// 	}

// }
// //add_action( 'woocommerce_checkout_order_processed', 'easoft_api_call' );

// /**
//  * Add a custom action to order actions select box on edit order page
//  * Only added for paid orders that haven't fired this action yet
//  *
//  * @param array $actions order actions array to display
//  * @return array - updated actions
//  */
// function easoft_add_order_meta_box_action( $actions ) {
// 	global $theorder;

// 	// bail if the order has been paid for or this action has been run.
// 	if ( ! $theorder->is_paid() || get_post_meta( $theorder->id, 'easoft_manual_order', true ) ) {
// 		return $actions;
// 	}

// 	// add "Easoft" custom action.
// 	$actions['easoft_action'] = __( 'Easoft', 'kouta_easoft' );
// 	return $actions;
// }
// add_action( 'woocommerce_order_actions', 'easoft_add_order_meta_box_action' );


// /**
//  * Add an order note when custom action is clicked
//  * Add a flag on the order to show it's been run
//  * 
//  * TODO: Maybe add cron job to retry if manual or automatic api call fails.
//  *
//  * @param WC_Order $order
//  */
// function easoft_process_order_meta_box_action( $order ) {

// 	// add the order note.
// 	$message = __( '(Manuaalinen) Tilaus luotu onnistuneesti Easoftiin', 'kouta_easoft' );
// 	$order->add_order_note( $message );
// 	easoft_api_call( $order->id );

// 	// add the flag so this action won't be shown again.
// 	update_post_meta( $order->id, 'easoft_manual_order', 'yes' );
// }
// add_action( 'woocommerce_order_action_easoft_action', 'easoft_process_order_meta_box_action' );
