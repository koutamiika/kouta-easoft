<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://koutamedia.fi
 * @since      1.0.0
 * @package woocommerce-easoft
 */

?>

<div class="wrap">

	<h1><?php esc_html_e( 'Easoft Asetukset', 'woocommerce-easoft' ); ?></h1>

	<form method="post" action="options.php">

		<?php settings_fields( 'woocommerce-easoft-settings' ); ?>
		<?php do_settings_sections( 'woocommerce-easoft-settings' ); ?>

		<table class="form-table">
			<tr valign="top">
				<th scope="row">Myyjän käyttäjätunnus <span class="required">*</span></th>
				<td><input type="text" class="widefat" name="woocommerce-easoft-seller-username" value="<?php echo esc_attr( get_option( 'woocommerce-easoft-seller-username' ) ); ?>" required="required" /><p class="description">Myyjän yksilöivä käyttäjätunnus, joka tulee löytyä Easoftin järjestelmästä.</strong></p></td>
			</tr>

			<tr valign="top">
				<th scope="row">Rajapinnan käyttäjätunnus <span class="required">*</span></th>
				<td><input type="text" class="widefat" name="woocommerce-easoft-api-username" value="<?php echo esc_attr( get_option( 'woocommerce-easoft-api-username' ) ); ?>" required="required" /><p class="description">Rajapinnan käyttäjätunnus.</strong></p></td>
			</tr>

			<tr valign="top">
				<th scope="row">Rajapinnan salasana <span class="required">*</span></th>
				<td><input type="password" class="widefat" name="woocommerce-easoft-api-password" value="<?php echo esc_attr( get_option( 'woocommerce-easoft-api-password' ) ); ?>" required="required" /><p class="description">Rajapinnan salasana.</strong></p></td>
			</tr>

			<tr valign="top">
				<th scope="row">Token <span class="required">*</span></th>
				<td><input type="password" class="widefat" name="woocommerce-easoft-api-token" value="<?php echo esc_attr( get_option( 'woocommerce-easoft-api-token' ) ); ?>" required="required" /><p class="description">Bearer token.</strong></p></td>
			</tr>

			<tr valign="top">
				<th scope="row">Rajapinnan URL</th>
				<td><input type="url" class="widefat" name="woocommerce-easoft-api-url" value="<?php echo esc_attr( trailingslashit( get_option( 'woocommerce-easoft-api-url' ) ) ); ?>" /></td>
			</tr>

			<tr valign="top">
				<th scope="row">Varaston ID</th>
				<td><input type="text" class="widefat" name="woocommerce-easoft-api-warehouse-id" value="<?php echo esc_attr( get_option( 'woocommerce-easoft-api-warehouse-id' ) ); ?>" /><p class="description">Varasto, jonka varastosaldoa käytetään synkronointiin. Syötä varaston ID.</strong></p></td>
			</tr>

		</table>

		<?php submit_button(); ?>

	</form>

</div>
