<?php
/**
 * Plugin main class
 *
 * @package woocommerce-easoft
 */

/**
 * Easoft class.
 */
class WooCommerce_Easoft {

	/**
	 * Username for API.
	 *
	 * @var string $api_user
	 */
	private $api_user;

	/**
	 * Password for API.
	 *
	 * @var string $api_pw
	 */
	private $api_pw;

	/**
	 * URL for API.
	 *
	 * @var string $api_url
	 */
	private $api_url;

	/**
	 * Bearer token.
	 *
	 * @var string $api_token
	 */
	private $api_token;

	/**
	 * ID of the warehouse to check stock balance from.
	 *
	 * @var string $warehouse_id
	 */
	public $warehouse_id;

	/**
	 * API Version.
	 *
	 * @var $api_version string
	 */
	private $api_version = 'v1';

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->api_user     = get_option( 'woocommerce-easoft-api-username' );
		$this->api_pw       = get_option( 'woocommerce-easoft-api-password' );
		$this->api_url      = get_option( 'woocommerce-easoft-api-url' );
		$this->api_token    = get_option( 'woocommerce-easoft-api-token' );
		$this->warehouse_id = get_option( 'woocommerce-easoft-warehouse-id' );

		$this->add_actions();
		$this->sync_schedule();

	}

	/**
	 * Register actions.
	 */
	public function add_actions() {

	}

	/**
	 * Schedule Cron
	 */
	public function sync_schedule() {
		if ( ! wp_next_scheduled( 'easoft_cron_hook' ) ) {
			wp_schedule_event( time(), 'every_15_minute', 'easoft_cron_hook' );
		}

		// Hook into that action that'll fire every hour.
		add_action( 'easoft_cron_hook', array( $this, 'sync_product_stock' ) );
	}

	/**
	 * Get brands from Easoft
	 *
	 * @return array $results array of brands
	 */
	public function get_easoft_brands() {

		$response = wp_remote_get(
			$this->api_url . 'api/' . $this->api_version . '/categories',
			array(
				'headers' => array(
					'Content-Type'  => 'application/json; charset=utf-8',
					'Authorization' => 'Bearer ' . $this->api_token,
				),
			)
		);

		$response_body = wp_remote_retrieve_body( $response );
		$results       = json_decode( $response_body, true );

		return $results;
	}

	/**
	 * Get products by category
	 *
	 * @param  int $cat_id category id.
	 * @return array $products array of product by category
	 */
	public function get_easoft_products_by_cat( $cat_id ) {

		$products_response = wp_remote_get(
			$this->api_url . 'api/' . $this->api_version . '/products?category=' . $cat_id,
			array(
				'headers' => array(
					'Content-Type'  => 'application/json; charset=utf-8',
					'Authorization' => 'Bearer ' . $this->api_token,
				),
			)
		);

		$products_response_body = wp_remote_retrieve_body( $products_response );
		$products               = json_decode( $products_response_body, true );

		return $products;

	}

	/**
	 * Get product stock from Easoft
	 *
	 * @param  int $product_id Easoft ID of the product.
	 * @return array $stock array of products stock
	 */
	public function get_easoft_product_stock( $product_id ) {

		$products_response = wp_remote_get(
			$this->api_url . 'api/' . $this->api_version . '/product_warehouse_balances/' . $product_id,
			array(
				'headers' => array(
					'Content-Type'  => 'application/json; charset=utf-8',
					'Authorization' => 'Bearer ' . $this->api_token,
				),
			)
		);

		$products_response_body = wp_remote_retrieve_body( $products_response );
		$stock                  = json_decode( $products_response_body, true );

		return $stock;

	}

	/**
	 * Get all products from Easoft
	 *
	 * @return array $easoft_products array of products
	 */
	public function get_easoft_products() {

		$results = $this->get_easoft_brands();

		// Array to store products.
		$easoft_products = array();

		if ( ! empty( $results['results'] ) ) {

			// Get Brands from Easoft.
			foreach ( $results['results'] as $cat ) {

				if ( ! empty( $cat['categories'] ) ) {

					// Get categories from Brands, if they exist.
					foreach ( $cat['categories'] as $subcat ) {

						$products = $this->get_easoft_products_by_cat( $subcat['id'] );

						foreach ( $products['results'] as $product ) {
							$easoft_products[] = array(
								'name' => $product['description'][0]['description'],
								'id'   => $product['id'],
								'sku'  => $product['item'],
							);
						}
					}
				}
			}
		}

		return $easoft_products;

	}

	/**
	 * Get published WooCommerce products.
	 *
	 * @return array $products array of published products.
	 */
	public function get_woocommerce_products() {

		$args     = array(
			'limit'  => -1,
			'status' => 'publish',
		);
		$query    = new WC_Product_Query( $args );
		$products = $query->get_products();

		return $products;
	}

	/**
	 * Syncronize product stock from Easoft.
	 */
	public function sync_product_stock() {

		$easoft_products = $this->get_easoft_products();
		$products        = $this->get_woocommerce_products();

		if ( $products ) {
			foreach ( $products as $product ) {

				$sku = $product->get_sku();

				// Check if SKU and Stock is set.
				if ( ! empty( $sku ) && is_numeric( $product->get_stock_quantity() ) ) {

					foreach ( $easoft_products as $easoft_product ) {

						// Get stock from Easoft only if SKU exists in WooCommerce.
						if ( $sku === $easoft_product['sku'] ) {

							$stock = $this->get_easoft_product_stock( $easoft_product['id'] );

							foreach ( $stock['results'][0]['balances'] as $warehouse ) {
								if ( $this->warehouse_id === $warehouse['warehouse']['id'] ) {

									wc_update_product_stock( $product, $warehouse['balance'], 'set' );

									if ( $warehouse['balance'] > 0 ) {
										$product->set_stock_status( 'instock' );
									} else {
										$product->set_stock_status( 'outofstock' );
									}

									$product->save();

								}
							}
						}
					}
				}
			}
		}

	}
}
