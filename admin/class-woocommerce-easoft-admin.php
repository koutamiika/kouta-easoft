<?php
/**
 * Admin functionality.
 *
 * Adds settings menu for the plugin.
 *
 * @package woocommerce-easoft
 */

/**
 * WooCommece Easoft Admin class
 */
class WooCommerce_Easoft_Admin {

	/**
	 * Class constructor
	 */
	public function __construct() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_menu', array( $this, 'register_menu_page' ) );
	}

	/**
	 * Register Kouta Easoft plugins menu
	 *
	 * @since 1.0.0
	 */
	public function register_menu_page() {
		add_submenu_page(
			'options-general.php',
			'Easoft Asetukset',
			'Easoft Asetukset',
			'manage_options',
			'woocommerce-easoft',
			array( $this, 'load_admin_page_content' ),
		);
	}

	/**
	 * Load admin menu view
	 */
	public function load_admin_page_content() {
		require_once EASOFT_PLUGIN_DIR . '/admin/partials/woocommerce-easoft-settings-view.php';
	}

	/**
	 * Register settings for the plugin
	 */
	public function register_settings() {
		register_setting(
			'woocommerce-easoft-settings',
			'woocommerce-easoft-seller-username',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'woocommerce-easoft-settings',
			'woocommerce-easoft-api-username',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'woocommerce-easoft-settings',
			'woocommerce-easoft-api-password',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'woocommerce-easoft-settings',
			'woocommerce-easoft-api-token',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
			)
		);
		register_setting(
			'woocommerce-easoft-settings',
			'woocommerce-easoft-api-url',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default'           => '',
			)
		);
		register_setting(
			'woocommerce-easoft-settings',
			'woocommerce-easoft-api-warehouse-id',
			array(
				'type'              => 'string',
				'show_in_rest'      => false,
				'sanitize_callback' => 'sanitize_text_field',
				'default'           => '',
			)
		);
	}
}

$admin = new WooCommerce_Easoft_Admin();
